<?php

    function diaSemana($dia) {
        switch ($dia) {
            case '1':
                echo "Es lunes :(";
                break;
            case '2':
                echo "Es martes aún :(";
                break;
            case '3':
                echo "¡Es miércoles ya!";
                break;
            case '4':
                echo "¡Vamos que ya es jueves!";
                break;
            case '5':
                echo "¡¡Por fin viernes!!";
                break;
            case '6':
                echo "Sábado, sabadete :D";
                break;
            case '7':
                echo "Oh, no... Ya es domingo...";
                break;
            default:
                echo "Eso no es un día de la semana, me temo.";
                break;

        }
    }

    diaSemana("6 \n");
?>